export default function(id, fields){
    return {
        watch: fields.reduce ((obj, field)=>{
            obj[field] = function(val) {
                localStorage.setItem(`${id}.${field}`, JSON.stringify(val))
            }
            return obj
        }, {}) ,
        methods: {
            saveAllPersistanData(){
                for(const field of fields){
                    const savedValue =  localStorage.setItem(`${id}.${field}`,JSON.stringify(this.$data[field]))
                }
            }
        },
        beforeDestroy() {
            this.saveAllPersistanData()
        },
        created() {
            for(const field of fields){
                const savedValue = localStorage.getItem(`${id}.${field}`)
                if(savedValue !== null){
                    this.$data[field] = JSON.parse(savedValue)
                }
            }
        },
    }
}