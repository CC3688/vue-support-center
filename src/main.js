import 'babel-polyfill'
import Vue from 'vue'
import state from './state.js'

//导入 全局组件
import './global-components'

//导入 插件
import VueFetch, { $fetch } from './plugins/fetch.js'
import VueState from './plugins/state.js'
Vue.use(VueFetch, {
    baseUrl: 'http://localhost:3000/'
})
Vue.use(VueState, state)


//导入时间过滤器
import * as filters from './filters'
for(const key in filters){
    Vue.filter(key, filters[key])
}

//导入 路由
import router from './router'

import AppLayout from './components/AppLayout.vue'

//初始化认证
async function main () {
    //获取用户信息
    try {
        state.user = await $fetch('user')
    } catch(e){
        console.warn(e)
    }
    //加载APP
    new Vue({
        el: '#app',
        data: state,
        render: h => h(AppLayout),
        router,
    })
}



main()