# vue-support-center
虚构的一个家公司的支持中心项目!

- 主页
- 认证系统
- 常见问题解答页面
- 工单管理页面
- 工单详情页
- 工单添加页

## 运行指南
1. clone 代码到本地后, 进入目录 运行 npm install
2. npm run dev
3. 进入 server 运行 npm install 完成后 运行 npm start

## 收获 小结
#### vue-router
1 启用 命名 路由 可以解藕
```
//router.js
{path: 'xxx', name: 'xxx', componenet: 'xxx'}
//在组件中使用
<router-link :to="{name: 'xxx'}" />

// 两个 name 相对应
```
2 激活状态 路由 组件会使用 router-link-active  CCS类,可以使用这个类名,更改其视觉效果

3 router-link 有exact 属性 是精确匹配的意思

4 {path: '*', ...} 通配符 路由 需要写在路由数组的最后

5 路由动画,过渡
```
     <transition name="fade" mode="out-in">
            <router-view></router-view>
     </transition>
```
6 路由元属性
```
{ path: '/login', name: 'login', component: Login, meta: {guest: true}}
通过元属性 meta 扩展路由器功能
```
7 路由守卫
```
router.beforeEach((to, from, next) => {
    // to.matched  是vue-router 提供的一个方法,得到的是一个数组, some 是es6数组的方法
    // 推荐 用 to.matched 这样的方法,可以避免 嵌套路由带了的一些问题
    if (to.matched.some(r=>r.meta.guest) && state.user) {
        console.log('entery')
        next({name: 'home'})
        return
    }
    next()    //这里一定要有next()  不然就坏了,路由出不去了,断了
    
})
```
8 动态路由  路由参数
```
// router.js 定义
{path: '/tickets/:id',name:'tickets', component: Ticket, props:route => ({id:route.params.id})}   
//参数的命名为 id

//组件中    
<router-link :to="{name:tickets}", params: {id: ticket._id}> 
//注意这里的params 就是传递给路由的参数了, key为参数的名, val为要传的值

//在 目标组件中 Ticket,  (this.$route.params.id  直接拿到)
//注意 路由定义 props 属性是告诉vue-router用props属将所有的路由参数作为props传递给它.
{path: '/tickets/:id',name:'tickets', component: Ticket, props:true} 
以下这种更灵活,
{path: '/tickets/:id',name:'tickets', component: Ticket, props:route => ({id:route.params.id})} 
以下这处是静态的,
{path: '/tickets/:id',name:'tickets', component: Ticket, props:{id: xxx}} 

```
9 滚动行为  scrollBehavior
路由器的history模式允许我们在路由改变时管理页面的滚动.可以每次将位置重置为最高的位置
或者更改路由之前的位置
```
const router = new VueRouter({
    routes,
    mode: 'history',
    scrollBehavior (to, from, savedPosition){
          if (savedPosition) {
            return savedPosition
          }
          if (to.hash) {
            return { selector: to.hash }
          }
          return { x: 0, y: 0 }
          //以上这个是重置到路由改变之前的位置 其它效果可以参考文档

    }
})
```
#### 扩展Vue --  自定义插件 plugins
```
//插件 必须要有一个 install 方法
export default {
    install(Vue, 参数配置options){

        Vue.prototype.$xxx = xxx
        //xxx 就是扩展你需要的 $ 开头是命名习惯
        // 其实,就是往Vue 原型 上添加东西
    }
}

// 使用 在main.js
import xxx from '...'  //上面插件的文件

Vue.use(xxx)

//在组件 中使用
 this.$xxx      //xxx 就是和上面定义的名字一样
```
#### mixins
其实就是代码的复用,有点像php 的use
mixins 文件里的代码 ,会被混合到使用它的组件里面去,当然重复声明的东西会产生覆盖
组件里的优先级高些
```
//mixins 文件   其实就是一个Vue一样, 有同样的选项可以供使用, 一般用下面这种方式,可以传参,更具灵活性
export default function (resources) {
    return {
        data () {
          
        },
        methods: {
          
        },
        created(){
           
        },
        computed: {
            
        },
    }
}

// 在组件中使用,从而可以不用重复写代码, 不会直接覆盖掉一个选项的,它会追加进去
<script>
import XXX from '../mixins/PersistantData'

export default {
    mixins:[
        XXX(实数)
    ],
    data(){
        return {
        
        }
    },
    computed: {
     
    },
    methods: {
       
    }
}
</script>

```
#### 表单
项目中把 input 组件化 利用 props 来个性化
同时在组件化时 添加上 v-bind="$attrs"   

然后在使用这个组件时,就可以直接 <组件名  rows  直接使用 input textarea 原有的属性了,而不需要通过属性props传过来设置了>

### localStorage   JSON.stringfy()
用来 缓存 用户在表单的输入, 防止用户刷新浏览器或其它原因丢失输入的内容

 ## 项目 运行 截图
 ![Alt text](./src/assets/image/1111111.png)
 ![Alt text](./src/assets/image/222222.png)
 ![Alt text](./src/assets/image/33333.png)
 ![Alt text](./src/assets/image/44444.png)